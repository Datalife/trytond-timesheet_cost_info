# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .timesheet import (
    TimesheetLine, HoursEmployee, HoursEmployeeWeekly, HoursEmployeeMonthly)


def register():
    Pool.register(
        TimesheetLine,
        HoursEmployee,
        HoursEmployeeWeekly,
        HoursEmployeeMonthly,
        module='timesheet_cost_info', type_='model')
