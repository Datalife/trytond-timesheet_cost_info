# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class TimesheetCostInfoTestCase(ModuleTestCase):
    """Test module"""
    module = 'timesheet_cost_info'

    def setUp(self):
        super(TimesheetCostInfoTestCase, self).setUp()


del ModuleTestCase
