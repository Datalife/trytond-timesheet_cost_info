# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from sql import Column
from sql.aggregate import Sum
from sql.functions import Extract

from trytond import backend
from trytond.model import fields
from trytond.modules.currency.fields import Monetary
from trytond.pool import PoolMeta


class TimesheetLine(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    cost_amount = fields.Function(
        Monetary('Cost amount', currency='cost_amount_currency',
            digits='cost_amount_currency'),
        'get_cost_amount')
    cost_amount_currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'get_cost_amount_currency_data')

    @classmethod
    def get_cost_amount_currency_data(cls, lines, names):
        res = {}
        for line in lines:
            for name in names:
                _name = name[21:]
                if not _name:
                    _name = 'id'
                res.setdefault(name, {})[line.id] = getattr(
                    line.employee.company.currency, _name)
        return res

    @classmethod
    def get_cost_amount(cls, lines, name=None):
        res = dict.fromkeys(list(map(int, lines)), 0)
        for line in lines:
            if line.duration and line.cost_price:
                res[line.id] = Decimal(
                    str(line.duration.total_seconds() / 3600)) * line.cost_price
        return res


class AmountTimesheetMixin(object):
    __slots__ = ()

    amount = Monetary('Amount', currency='currency', digits='currency')
    currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'get_currency')

    @classmethod
    def table_query(cls):
        query = super().table_query()
        line = (query.from_[0] if isinstance(query.from_[0].left, Column)
            else query.from_[0].left)
        columns = list(query.columns)
        if backend.name != 'sqlite':
            columns.append(
                Sum(line.cost_price
                    * (Extract('HOURS', line.duration)
                        + (Extract('DAYS', line.duration) * 24)
                        + (Extract('MINUTE', line.duration) / 60))
                    ).as_('amount'))
        else:
            columns.append(
                Sum(line.cost_price * (line.duration / 3600)).as_('amount'))

        query.columns = columns
        return query

    @classmethod
    def get_currency(cls, records, names):
        res = {n: {} for n in names}
        for record in records:
            for name in names:
                _name = name[9:] or 'id'
                res[name][record.id] = getattr(record.employee.company
                    and record.employee.company.currency, _name)
        return res


class HoursEmployee(AmountTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee'


class HoursEmployeeWeekly(AmountTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee_weekly'


class HoursEmployeeMonthly(AmountTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee_monthly'
